# Assignment 1
InApp Week 1 Assignment

Make a two-player Rock-Paper-Scissors game. One of the player is the computer
10 rounds. Print out the winner and points earned by both the players at the end of the game.

Remember the rules:
Rock beats Scissors
Scissors beats Paper
Paper bears Rock

Use a dictionary to store the history of choices made by the Player and Computer.
You should be able to print the choices made by the Player and Computer in each round once the game is completed.

Ex-
Enter the round for which you need information >>3
Output
Player choice = Rock
Computer choice = Paper
Player won Round 3
