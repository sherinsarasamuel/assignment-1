# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
from random import randint

#create a list of play options
t = ["Rock", "Paper", "Scissors"]

#assign a random play to the computer
computer = t[randint(0,2)]
result_dict={}
player_final=0
computer_final=0

r=1
while (r <=10):
#input from player
    player = input("Rock, Paper, Scissors? ")
    if player == computer:
        result="Tie"
    elif player == "Rock":
        if computer == "Paper":
            result="Computer"
        else:
            result="Player"
    elif player == "Paper":
        if computer == "Scissors":
            result="Computer"
        else:
            result="Player"
    elif player == "Scissors":
        if computer == "Rock":
            result="Computer"
        else:
            result="Player"

   #if invalid input is given     
    else:
        print("That's not a valid play. Check your spelling!")
        
    #storing play history in dictionary  
    if result=='Player':
        player_final=player_final+1
    elif result=='Computer':
        computer_final=computer_final+1
    result_dict[r]=[computer,player,result]
    r=r+1    
    computer = t[randint(0,2)]

#final output statements
print("Player won ", player_final," times.")
print("Computer won ", computer_final," times")
tie=10-(player_final+computer_final)
print("Game tied ", tie," times")
check=int(input("Enter the round for which you need the information : "))
print("Player choice= ",result_dict[check][1])
print("Computer Choice= ",result_dict[check][0])

soln=result_dict[check][2]
if soln == 'Tie':
    print("Tie")
else:
    print(result_dict[check][2]," won round ", check)  

 